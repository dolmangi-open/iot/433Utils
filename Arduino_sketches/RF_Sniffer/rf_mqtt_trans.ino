/*
  RF_Sniffer
  
  Hacked from https://github.com/sui77/rc-switch
  
  by @justy to provide a handy RF code sniffer
  
  dolmangi@naver.com modified
  
*/
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//----- WiFi settings
const char *ssid = "dolmangi";
const char *password = "";
const char* mqtt_server = "192.168.0.93";
const char* mqtt_topic = "myhome/living/switch1";
char* mqtt_message = "1";


IPAddress ip_static(192, 168, 0, 87);
IPAddress ip_gateway(192, 168, 0, 1);
IPAddress ip_subnet(255, 255, 255, 0);
IPAddress ip_dns(192, 168, 0, 1);


#include "RCSwitch.h"
RCSwitch mySwitch = RCSwitch();



WiFiClient espClient;
PubSubClient client(espClient);
void sendmessage(unsigned long swno, int state);

void setup() 
{
  Serial.begin(115200);
  Serial.print("Hello!!");
  
  pinMode(BUILTIN_LED, OUTPUT);

  WiFi.config(ip_static, ip_gateway, ip_subnet, ip_dns);
  WiFi.begin(ssid, password);
  
  Serial.println("Connecting to AP");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  //WiFi.forceSleepBegin();   
  
  mySwitch.enableReceive(13);  // Receiver on interrupt 0 => that is pin #2
}

#define COUNTER_MAX 100
unsigned long last_button=0;
long first_millis=0;
long last_millis = 0;
void loop() 
{
  
  long now = millis();
  if (last_button > 0 && now - last_millis > 200) 
  {
    int duration = (now-first_millis)/200;
    if (duration>9) duration=9;
    
    sendmessage(last_button,duration);        
    last_button=0;
    
    //WiFi.forceSleepBegin();   
    client.disconnect();
    digitalWrite(BUILTIN_LED, HIGH);
  }

  
  if (mySwitch.available()) 
  {    
    int value = mySwitch.getReceivedValue();
    
    if (value == 0) 
    {
      Serial.print("Unknown encoding");
    } 
    else 
    {
      Serial.print("Received ");
      Serial.print( value);
      Serial.print(" / ");
      Serial.print( mySwitch.getReceivedBitlength() );
      Serial.print("bit ");
      Serial.print("Protocol: ");
      Serial.println( mySwitch.getReceivedProtocol() );

      if (last_button == 0)
      {
        last_button = value;
        last_millis=now;
        first_millis=last_millis;
/*
        WiFi.forceSleepWake();
        Serial.println("Connecting to AP");
        while (WiFi.status() != WL_CONNECTED)
        {
          delay(100);
          Serial.print(".");
        }
      
        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
  */      
        digitalWrite(BUILTIN_LED, LOW);
        if (!client.connected()) 
          reconnect();
        
        sendmessage(value,0);        
      }
      else if (last_button == value)
      {
        last_millis=now;    
      }

    }
    mySwitch.resetAvailable();
  }
  else
  {
    delay(100);
  }
}

void reconnect() 
{
  // Loop until we're reconnected
  int retry = 0;
  while (!client.connected()) 
  {
    Serial.print("connecting MQTT...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) 
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
//      client.publish(mqtt_topic, "hello world");
      // ... and resubscribe
//      client.subscribe(mqtt_topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 0.5 seconds");
      // Wait 500 milli seconds before retrying
      delay(500);
      if (retry++ >3)
        ESP.reset();
        
    }
  }
}

void sendmessage(unsigned long swno, int state)
{
      char msg[50];
      
      snprintf (msg, 75, "%d %d" ,state, swno );
      client.publish("myhome/button_433", msg);
      Serial.print("myhome/button_433  " );
      Serial.println( msg );      
      client.loop();


}


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");


  String msgText = "=> ";

  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    msgText += (char)payload[i];
  }
  Serial.println();
}


